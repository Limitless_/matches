from gui.elements.Window import Window


def open_game_window():
    window = Window()
    window.mainloop()


if __name__ == '__main__':
    open_game_window()
