# Game - Matches
* There are 2 piles of matches. 
* Players each turn can take any number of matches from one pile.
* Player who has no matches to take from any of the piles - loses.

# Development

## Used technologies
* Python 3.10
* Tkinter 8.6
* PyInstaller 5.9.0 (for the .exe creation)

## IDE
* PyCharm 2023.3.3

# Algorithms
* Recursive game tree generation
* Minimax algorithm