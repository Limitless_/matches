from gameplay.enum.PlayerEnum import PlayerEnum


def is_even(num):
    return num % 2 == 0


# Minimax
def condition_for_max(node, who_starts):
    return (is_even(node.level) and who_starts == PlayerEnum.AI) or (
            not is_even(node.level) and who_starts == PlayerEnum.PLAYER)


# UI
def ai_first_odd_turn(game_session):
    return game_session.first_turn == PlayerEnum.AI and not is_even(game_session.turn_number)


def player_first_even_turn(game_session):
    return game_session.first_turn == PlayerEnum.PLAYER and is_even(game_session.turn_number)