from operator import attrgetter
from tkinter import Tk, Button
from tkinter.ttk import Label, Entry

from utils.Utils import ai_first_odd_turn, player_first_even_turn
from ai.tree.GameTree import GameTree
from ai.tree.Node import Node
from gameplay.GameSession import GameSession
from gameplay.enum.PlayerEnum import PlayerEnum


# main class for Tkinter GUI containing all elements
class Window(Tk):
    def __init__(self):
        super().__init__()

        # window
        self.title("Matches")
        self.geometry("500x150")

        # main menu
        self.counter_max_label = ""
        self.first_counter_input_entry = None
        self.second_counter_input_entry = None
        self.start_game_button_ai_starts = None
        self.start_game_button_player_starts = None

        # game
        self.firstCounterLabel = ""
        self.secondCounterLabel = ""

        self.firstEntry = None
        self.secondEntry = None

        self.firstEntrySubmitButton = None
        self.secondEntrySubmitButton = None

        # divide grid in equal parts
        for i in range(2):
            self.grid_columnconfigure(i, weight=1)

        # game end
        self.end_game_label = ""
        self.end_game_label_winner = ""
        self.restart_game_button = None

        self.open_main_menu()

    # show UI elements in main menu
    def open_main_menu(self):
        # create labels
        self.counter_max_label = Label(self, text="Enter starting counter numbers:")

        # create entries
        self.first_counter_input_entry = Entry()
        self.second_counter_input_entry = Entry()

        # create buttons to submit entries
        self.start_game_button_ai_starts = Button(self,
                                                  text="AI starts",
                                                  command=lambda: self.start_game(PlayerEnum.AI))

        self.start_game_button_player_starts = Button(self,
                                                      text="You start",
                                                      command=lambda: self.start_game(PlayerEnum.PLAYER))

        # put labels into the grid
        self.counter_max_label.grid(row=0, column=0, columnspan=2)

        # put entries into the grid
        self.first_counter_input_entry.grid(row=1, column=0)
        self.second_counter_input_entry.grid(row=1, column=1)

        # put buttons into the grid
        self.start_game_button_ai_starts.grid(row=2, column=0, columnspan=2)
        self.start_game_button_player_starts.grid(row=3, column=0, columnspan=2)

    # game's main method
    def start_game(self, first_turn):
        # init game session
        game_session = self.fill_data_from_main_menu(first_turn)

        first_node = Node(game_session.counters, 1)
        game_tree = GameTree(first_node, first_turn)
        game_tree.generate_tree_first(first_node)

        self.clear_window()

        # show UI elements during the game
        # create labels
        self.firstCounterLabel = Label(self, text=str(game_session.counters[0]))
        self.secondCounterLabel = Label(self, text=str(game_session.counters[1]))

        # create entries
        self.firstEntry = Entry()
        self.secondEntry = Entry()

        # create buttons to submit entries
        self.firstEntrySubmitButton = Button(self, text="Take", command=lambda:
                                             self.make_turn(self.firstEntrySubmitButton,
                                                            self.firstEntry,
                                                            self.firstCounterLabel,
                                                            game_session,
                                                            game_tree,
                                                            counter_number=0))

        self.secondEntrySubmitButton = Button(self, text="Take", command=lambda:
                                              self.make_turn(self.secondEntrySubmitButton,
                                                             self.secondEntry,
                                                             self.secondCounterLabel,
                                                             game_session,
                                                             game_tree,
                                                             counter_number=1))

        self.restart_game_button = Button(self, text="Restart", command=self.restart)

        # put labels into the grid
        self.firstCounterLabel.grid(row=0, column=0)
        self.secondCounterLabel.grid(row=0, column=1)

        # put entries into the grid
        self.firstEntry.grid(row=1, column=0)
        self.secondEntry.grid(row=1, column=1)

        # put buttons into the grid
        self.firstEntrySubmitButton.grid(row=2, column=0)
        self.secondEntrySubmitButton.grid(row=2, column=1)
        self.restart_game_button.grid(row=5, column=0, columnspan=2)

        # not the best way to make first AI turn
        if game_session.first_turn == PlayerEnum.AI:
            self.make_ai_turn(game_session, game_tree)

    def make_ai_turn(self, game_session, game_tree):
        next_moves = []

        # get all next possible moves for the AI
        if ai_first_odd_turn(game_session):
            if game_session.ai_current_node is None:
                next_moves = game_tree.existing_nodes[0].children
            else:
                for child in game_session.ai_current_node.children:
                    if child.counters[0] == game_session.counters[0] and child.counters[1] == game_session.counters[1]:
                        next_moves = child.children
        else:
            for node in game_tree.existing_nodes:
                if node.counters[0] == game_session.counters[0] and node.counters[1] == game_session.counters[1]:
                    next_moves = node.children

        # AI outcome is 1 per OutcomeEnum, i.e., maximum among scores
        next_move = max(next_moves, key=attrgetter('score'))

        # decide which entry should be inputted into
        if next_move.counters[0] != game_session.counters[0]:
            self.firstEntry.insert(0, game_session.counters[0] - next_move.counters[0])
            self.make_turn(self.firstEntrySubmitButton,
                           self.firstEntry,
                           self.firstCounterLabel,
                           game_session,
                           game_tree,
                           counter_number=0)
        else:
            self.secondEntry.insert(0, game_session.counters[1] - next_move.counters[1])
            self.make_turn(self.secondEntrySubmitButton,
                           self.secondEntry,
                           self.secondCounterLabel,
                           game_session,
                           game_tree,
                           counter_number=1)

        game_session.ai_current_node = next_move

    # perform turn and update according UI elements
    def make_turn(self, button, entry, counter_label, game_session, game_tree, counter_number):
        user_input = entry.get()

        if user_input.isnumeric():
            if game_session.take(counter_number, int(user_input)):
                # reduce counter
                counter_label_after_turn = game_session.counters[counter_number]
                counter_label["text"] = counter_label_after_turn
                entry.delete(0, 'end')

                # remove entry if empty
                if counter_label_after_turn == 0:
                    entry.destroy()
                    button.destroy()

                # check for endgame and make AI turn if possible
                if all(c == 0 for c in game_session.counters):
                    self.end_game(game_session)
                elif ai_first_odd_turn(game_session) or player_first_even_turn(game_session):
                    self.make_ai_turn(game_session, game_tree)

    def end_game(self, game_session):
        # create labels
        self.end_game_label = Label(self, text="Game ended")

        if ai_first_odd_turn(game_session) or player_first_even_turn(game_session):
            winner = "You"
        else:
            winner = "AI"
        self.end_game_label_winner = Label(self, text=winner + " won!")

        # create buttons
        self.restart_game_button = Button(self, text="Restart", command=self.restart)

        # put labels into the grid
        self.end_game_label.grid(row=3, column=0, columnspan=2)
        self.end_game_label_winner.grid(row=4, column=0, columnspan=2)

        # put buttons into the grid
        self.restart_game_button.grid(row=5, column=0, columnspan=2)

    def clear_window(self):
        for widgets in self.winfo_children():
            widgets.destroy()

    def restart(self):
        self.clear_window()
        self.open_main_menu()

    def fill_data_from_main_menu(self, first_turn):
        counters = [int(self.first_counter_input_entry.get()), int(self.second_counter_input_entry.get())]
        return GameSession(counters, first_turn)
