# class with the logic for the game and game session flow
class GameSession:
    def __init__(self, counters, first_turn):
        self.counters = counters
        self.first_turn = first_turn
        self.ai_current_node = None
        self.turn_number = 1

    def take(self, counter_number, points_to_take):
        if points_to_take > 0:
            points_before_turn = self.counters[counter_number]
            points_after_turn = points_before_turn - points_to_take

            if points_after_turn >= 0:
                self.turn_number += 1

                self.counters[counter_number] = points_after_turn
                return True
        else:
            return False
