from enum import Enum


# First turn enum
class PlayerEnum(Enum):
    AI = 1
    PLAYER = -1
