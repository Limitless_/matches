# class for the game tree node
class Node:
    def __init__(self, counters, level):
        self.counters = counters
        self.level = level
        self.children = []
        self.score = None

    # necessary for the correct 'in' operator work in GameTree.py tree generation
    # equal nodes are nodes on the same level with the same counters
    # is used to prevent duplicates and share the children for the same nodes
    def __eq__(self, other):
        return self.level == other.level and self.counters == other.counters
