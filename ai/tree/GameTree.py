from ai.minimax.Minimax import minimax_evaluate_node
from ai.tree.Node import Node


# class containing game tree
class GameTree:
    def __init__(self, node, first_turn):
        self.top = node
        self.existing_nodes = []
        self.first_turn = first_turn

    # start tree generation with the first node
    def generate_tree_first(self, top):
        self.existing_nodes.append(top)
        self.generate_tree(top)

    # recursive tree generation
    def generate_tree(self, node):
        counters = node.counters

        # iterate through each counter
        for counter in range(len(counters)):
            if counters[counter] > 0:
                # subtract every possible number
                i = 1
                while counters[counter] - i >= 0:
                    child_counters = counters.copy()
                    child_counters[counter] -= i

                    # recursion goes down
                    child = Node(child_counters, node.level + 1)

                    # recursion goes up
                    # prevent duplicates in the tree
                    if child not in self.existing_nodes:
                        node.children.append(child)
                        self.existing_nodes.append(child)
                        self.generate_tree(child)
                    else:
                        for n in self.existing_nodes:
                            if n.level == child.level and n.counters == child.counters:
                                node.children.append(n)
                    i += 1

        # evaluate the node using Minimax algorithm before moving away from the node
        minimax_evaluate_node(self.first_turn, node)

        # debug information about each tree node (nodes are outputted in postorder)
        print("Level: " + str(node.level))
        print("Counters: " + str(node.counters))
        print("Score: " + str(node.score))
        print()
