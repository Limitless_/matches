from enum import Enum


# Minimax node score
class OutcomeEnum(Enum):
    MAX = 1
    MIN = -1
