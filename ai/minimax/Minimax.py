from operator import attrgetter

from utils.Utils import condition_for_max
from ai.minimax.enum.OutcomeEnum import OutcomeEnum


# set score for the node based on the Minimax algorithm
def minimax_evaluate_node(who_starts, node):
    if len(node.children) == 0:
        # end-node
        if condition_for_max(node, who_starts):
            node.score = OutcomeEnum.MAX.value
        else:
            node.score = OutcomeEnum.MIN.value
    else:
        # in-between node
        if condition_for_max(node, who_starts):
            node.score = min(node.children, key=attrgetter('score')).score
        else:
            node.score = max(node.children, key=attrgetter('score')).score
